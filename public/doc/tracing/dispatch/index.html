<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="generator" content="rustdoc"><meta name="description" content="Dispatches trace events to a `Collect`."><title>tracing::dispatch - Rust</title><script>if(window.location.protocol!=="file:")document.head.insertAdjacentHTML("beforeend","SourceSerif4-Regular-46f98efaafac5295.ttf.woff2,FiraSans-Regular-018c141bf0843ffd.woff2,FiraSans-Medium-8f9a781e4970d388.woff2,SourceCodePro-Regular-562dcc5011b6de7d.ttf.woff2,SourceCodePro-Semibold-d899c5a5c4aeb14a.ttf.woff2".split(",").map(f=>`<link rel="preload" as="font" type="font/woff2" crossorigin href="../../static.files/${f}">`).join(""))</script><link rel="stylesheet" href="../../static.files/normalize-76eba96aa4d2e634.css"><link rel="stylesheet" href="../../static.files/rustdoc-405f8b29f52305f8.css"><meta name="rustdoc-vars" data-root-path="../../" data-static-root-path="../../static.files/" data-current-crate="tracing" data-themes="" data-resource-suffix="" data-rustdoc-version="1.83.0-nightly (26b5599e4 2024-09-06)" data-channel="nightly" data-search-js="search-a99f1315e7cc5121.js" data-settings-js="settings-7e3bb6c46e92e77c.js" ><script src="../../static.files/storage-29b1d5a9048d38fe.js"></script><script defer src="../sidebar-items.js"></script><script defer src="../../static.files/main-14659ec02b58af51.js"></script><noscript><link rel="stylesheet" href="../../static.files/noscript-40f72c9259523cb3.css"></noscript><link rel="icon" href="https://raw.githubusercontent.com/tokio-rs/tracing/master/assets/favicon.ico"></head><body class="rustdoc mod"><!--[if lte IE 11]><div class="warning">This old browser is unsupported and will most likely display funky things.</div><![endif]--><nav class="mobile-topbar"><button class="sidebar-menu-toggle" title="show sidebar"></button><a class="logo-container" href="../../tracing/index.html"><img src="https://raw.githubusercontent.com/tokio-rs/tracing/master/assets/logo-type.png" alt=""></a></nav><nav class="sidebar"><div class="sidebar-crate"><a class="logo-container" href="../../tracing/index.html"><img src="https://raw.githubusercontent.com/tokio-rs/tracing/master/assets/logo-type.png" alt="logo"></a><h2><a href="../../tracing/index.html">tracing</a><span class="version">0.2.0</span></h2></div><div class="sidebar-elems"><section id="rustdoc-toc"><h2 class="location"><a href="#">Module dispatch</a></h2><h3><a href="#">Sections</a></h3><ul class="block top-toc"><li><a href="#using-the-trace-dispatcher" title="Using the Trace Dispatcher">Using the Trace Dispatcher</a><ul><li><a href="#setting-the-default-collector" title="Setting the Default Collector">Setting the Default Collector</a></li><li><a href="#accessing-the-default-collector" title="Accessing the Default Collector">Accessing the Default Collector</a></li></ul></li></ul><h3><a href="#structs">Module Items</a></h3><ul class="block"><li><a href="#structs" title="Structs">Structs</a></li><li><a href="#functions" title="Functions">Functions</a></li></ul></section><div id="rustdoc-modnav"><h2 class="in-crate"><a href="../index.html">In crate tracing</a></h2></div></div></nav><div class="sidebar-resizer"></div><main><div class="width-limiter"><rustdoc-search></rustdoc-search><section id="main-content" class="content"><div class="main-heading"><h1>Module <a href="../index.html">tracing</a>::<wbr><a class="mod" href="#">dispatch</a><button id="copy-path" title="Copy item path to clipboard">Copy item path</button></h1><span class="out-of-band"><a class="src" href="../../src/tracing/dispatch.rs.html#1-160">source</a> · <button id="toggle-all-docs" title="collapse all docs">[<span>&#x2212;</span>]</button></span></div><details class="toggle top-doc" open><summary class="hideme"><span>Expand description</span></summary><div class="docblock"><p>Dispatches trace events to a <a href="../trait.Collect.html" title="trait tracing::Collect"><code>Collect</code></a>.</p>
<p>The <em>dispatcher</em> is the component of the tracing system which is responsible
for forwarding trace data from the instrumentation points that generate it
to the collector that collects it.</p>
<h2 id="using-the-trace-dispatcher"><a class="doc-anchor" href="#using-the-trace-dispatcher">§</a>Using the Trace Dispatcher</h2>
<p>Every thread in a program using <code>tracing</code> has a <em>default collector</em>. When
events occur, or spans are created, they are dispatched to the thread’s
current collector.</p>
<h3 id="setting-the-default-collector"><a class="doc-anchor" href="#setting-the-default-collector">§</a>Setting the Default Collector</h3>
<p>By default, the current collector is an empty implementation that does
nothing. Trace data provided to this “do nothing” implementation is
immediately discarded, and is not available for any purpose.</p>
<p>To use another collector implementation, it must be set as the default.
There are two methods for doing so: [<code>with_default</code>] and
<a href="fn.set_global_default.html" title="fn tracing::dispatch::set_global_default"><code>set_global_default</code></a>. <code>with_default</code> sets the default collector for the
duration of a scope, while <code>set_global_default</code> sets a default collector
for the entire process.</p>
<p>To use either of these functions, we must first wrap our collector in a
<a href="../struct.Dispatch.html" title="struct tracing::Dispatch"><code>Dispatch</code></a>, a cloneable, type-erased reference to a collector. For
example:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered"><code><span class="kw">use </span>dispatch::Dispatch;

<span class="kw">let </span>my_collector = FooCollector::new();
<span class="kw">let </span>my_dispatch = Dispatch::new(my_collector);</code></pre></div>
<p>Then, we can use [<code>with_default</code>] to set our <code>Dispatch</code> as the default for
the duration of a block:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered"><code><span class="comment">// no default collector

</span>dispatch::with_default(<span class="kw-2">&amp;</span>my_dispatch, || {
    <span class="comment">// my_collector is the default
</span>});

<span class="comment">// no default collector again</span></code></pre></div>
<p>It’s important to note that <code>with_default</code> will not propagate the current
thread’s default collector to any threads spawned within the <code>with_default</code>
block. To propagate the default collector to new threads, either use
<code>with_default</code> from the new thread, or use <code>set_global_default</code>.</p>
<p>As an alternative to <code>with_default</code>, we can use <a href="fn.set_global_default.html" title="fn tracing::dispatch::set_global_default"><code>set_global_default</code></a> to
set a <code>Dispatch</code> as the default for all threads, for the lifetime of the
program. For example:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered"><code><span class="comment">// no default collector

</span>dispatch::set_global_default(my_dispatch)
    <span class="comment">// `set_global_default` will return an error if the global default
    // collector has already been set.
    </span>.expect(<span class="string">"global default was already set!"</span>);

<span class="comment">// `my_collector` is now the default</span></code></pre></div>
<div class="example-wrap" style="display:inline-block">
<pre class="ignore" style="white-space:normal;font:inherit;">
<p><strong>Note</strong>: The thread-local scoped dispatcher (<code>with_default</code>)
requires the Rust standard library. <code>no_std</code> users should
use <a href="fn.set_global_default.html" title="fn tracing::dispatch::set_global_default"><code>set_global_default()</code></a> instead.</p>
<p></pre></div></p>
<h3 id="accessing-the-default-collector"><a class="doc-anchor" href="#accessing-the-default-collector">§</a>Accessing the Default Collector</h3>
<p>A thread’s current default collector can be accessed using the
<a href="fn.get_default.html" title="fn tracing::dispatch::get_default"><code>get_default</code></a> function, which executes a closure with a reference to the
currently default <code>Dispatch</code>. This is used primarily by <code>tracing</code>
instrumentation.</p>
</div></details><h2 id="structs" class="section-header">Structs<a href="#structs" class="anchor">§</a></h2><ul class="item-table"><li><div class="item-name"><a class="struct" href="struct.Dispatch.html" title="struct tracing::dispatch::Dispatch">Dispatch</a></div><div class="desc docblock-short"><code>Dispatch</code> trace data to a <a href="../trait.Collect.html" title="trait tracing::Collect"><code>Collect</code></a>.</div></li><li><div class="item-name"><a class="struct" href="struct.SetGlobalDefaultError.html" title="struct tracing::dispatch::SetGlobalDefaultError">SetGlobal<wbr>Default<wbr>Error</a></div><div class="desc docblock-short">Returned if setting the global dispatcher fails.</div></li><li><div class="item-name"><a class="struct" href="struct.WeakDispatch.html" title="struct tracing::dispatch::WeakDispatch">Weak<wbr>Dispatch</a></div><div class="desc docblock-short"><code>WeakDispatch</code> is a version of <a href="../struct.Dispatch.html" title="struct tracing::Dispatch"><code>Dispatch</code></a> that holds a non-owning reference
to a <a href="../trait.Collect.html" title="trait tracing::Collect">collector</a>.</div></li></ul><h2 id="functions" class="section-header">Functions<a href="#functions" class="anchor">§</a></h2><ul class="item-table"><li><div class="item-name"><a class="fn" href="fn.get_default.html" title="fn tracing::dispatch::get_default">get_<wbr>default</a></div><div class="desc docblock-short">Executes a closure with a reference to the current <a href="super::dispatcher::Dispatch">dispatcher</a>.</div></li><li><div class="item-name"><a class="fn" href="fn.set_global_default.html" title="fn tracing::dispatch::set_global_default">set_<wbr>global_<wbr>default</a></div><div class="desc docblock-short">Sets this dispatch as the global default for the duration of the entire program.
Will be used as a fallback if no thread-local dispatch has been set in a thread
(using <code>with_default</code>.)</div></li></ul></section></div></main></body></html>