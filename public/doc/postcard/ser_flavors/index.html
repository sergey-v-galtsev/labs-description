<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="generator" content="rustdoc"><meta name="description" content="Serialization Flavors"><title>postcard::ser_flavors - Rust</title><script>if(window.location.protocol!=="file:")document.head.insertAdjacentHTML("beforeend","SourceSerif4-Regular-46f98efaafac5295.ttf.woff2,FiraSans-Regular-018c141bf0843ffd.woff2,FiraSans-Medium-8f9a781e4970d388.woff2,SourceCodePro-Regular-562dcc5011b6de7d.ttf.woff2,SourceCodePro-Semibold-d899c5a5c4aeb14a.ttf.woff2".split(",").map(f=>`<link rel="preload" as="font" type="font/woff2" crossorigin href="../../static.files/${f}">`).join(""))</script><link rel="stylesheet" href="../../static.files/normalize-76eba96aa4d2e634.css"><link rel="stylesheet" href="../../static.files/rustdoc-405f8b29f52305f8.css"><meta name="rustdoc-vars" data-root-path="../../" data-static-root-path="../../static.files/" data-current-crate="postcard" data-themes="" data-resource-suffix="" data-rustdoc-version="1.83.0-nightly (26b5599e4 2024-09-06)" data-channel="nightly" data-search-js="search-a99f1315e7cc5121.js" data-settings-js="settings-7e3bb6c46e92e77c.js" ><script src="../../static.files/storage-29b1d5a9048d38fe.js"></script><script defer src="../sidebar-items.js"></script><script defer src="../../static.files/main-14659ec02b58af51.js"></script><noscript><link rel="stylesheet" href="../../static.files/noscript-40f72c9259523cb3.css"></noscript><link rel="alternate icon" type="image/png" href="../../static.files/favicon-32x32-422f7d1d52889060.png"><link rel="icon" type="image/svg+xml" href="../../static.files/favicon-2c020d218678b618.svg"></head><body class="rustdoc mod"><!--[if lte IE 11]><div class="warning">This old browser is unsupported and will most likely display funky things.</div><![endif]--><nav class="mobile-topbar"><button class="sidebar-menu-toggle" title="show sidebar"></button></nav><nav class="sidebar"><div class="sidebar-crate"><h2><a href="../../postcard/index.html">postcard</a><span class="version">1.0.10</span></h2></div><div class="sidebar-elems"><section id="rustdoc-toc"><h2 class="location"><a href="#">Module ser_<wbr>flavors</a></h2><h3><a href="#">Sections</a></h3><ul class="block top-toc"><li><a href="#serialization-flavors" title="Serialization Flavors">Serialization Flavors</a><ul><li><a href="#usability" title="Usability">Usability</a></li><li><a href="#when-to-use-multiple-flavors" title="When to use (multiple) flavors">When to use (multiple) flavors</a></li><li><a href="#when-not-to-use-multiple-flavors" title="When NOT to use (multiple) flavors">When NOT to use (multiple) flavors</a></li><li><a href="#examples" title="Examples">Examples</a></li></ul></li></ul><h3><a href="#structs">Module Items</a></h3><ul class="block"><li><a href="#structs" title="Structs">Structs</a></li><li><a href="#traits" title="Traits">Traits</a></li></ul></section><div id="rustdoc-modnav"><h2 class="in-crate"><a href="../index.html">In crate postcard</a></h2></div></div></nav><div class="sidebar-resizer"></div><main><div class="width-limiter"><rustdoc-search></rustdoc-search><section id="main-content" class="content"><div class="main-heading"><h1>Module <a href="../index.html">postcard</a>::<wbr><a class="mod" href="#">ser_flavors</a><button id="copy-path" title="Copy item path to clipboard">Copy item path</button></h1><span class="out-of-band"><a class="src" href="../../src/postcard/ser/flavors.rs.html#1-722">source</a> · <button id="toggle-all-docs" title="collapse all docs">[<span>&#x2212;</span>]</button></span></div><details class="toggle top-doc" open><summary class="hideme"><span>Expand description</span></summary><div class="docblock"><h2 id="serialization-flavors"><a class="doc-anchor" href="#serialization-flavors">§</a>Serialization Flavors</h2>
<p>“Flavors” in <code>postcard</code> are used as modifiers to the serialization or deserialization
process. Flavors typically modify one or both of the following:</p>
<ol>
<li>The output medium of the serialization, e.g. whether the data is serialized to a <code>[u8]</code> slice, or a <code>heapless::Vec</code>.</li>
<li>The format of the serialization, such as encoding the serialized output in a COBS format, performing CRC32 checksumming while serializing, etc.</li>
</ol>
<p>Flavors are implemented using the <a href="trait.Flavor.html" title="trait postcard::ser_flavors::Flavor"><code>Flavor</code></a> trait, which acts as a “middleware” for receiving the bytes as serialized by <code>serde</code>.
Multiple flavors may be combined to obtain a desired combination of behavior and storage.
When flavors are combined, it is expected that the storage flavor (such as <code>Slice</code> or <code>HVec</code>) is the innermost flavor.</p>
<p>Custom flavors may be defined by users of the <code>postcard</code> crate, however some commonly useful flavors have been provided in
this module. If you think your custom flavor would be useful to others, PRs adding flavors are very welcome!</p>
<h3 id="usability"><a class="doc-anchor" href="#usability">§</a>Usability</h3>
<p>Flavors may not always be convenient to use directly, as they may expose some implementation details of how the
inner workings of the flavor behaves. It is typical to provide a convenience method for using a flavor, to prevent
the user from having to specify generic parameters, setting correct initialization values, or handling the output of
the flavor correctly. See <code>postcard::to_vec()</code> for an example of this.</p>
<p>It is recommended to use the <a href="../fn.serialize_with_flavor.html"><code>serialize_with_flavor()</code></a> method for serialization. See it’s documentation for information
regarding usage and generic type parameters.</p>
<h3 id="when-to-use-multiple-flavors"><a class="doc-anchor" href="#when-to-use-multiple-flavors">§</a>When to use (multiple) flavors</h3>
<p>Combining flavors are nice for convenience, as they perform potentially multiple steps of
serialization at one time.</p>
<p>This can often be more memory efficient, as intermediate buffers are not typically required.</p>
<h3 id="when-not-to-use-multiple-flavors"><a class="doc-anchor" href="#when-not-to-use-multiple-flavors">§</a>When NOT to use (multiple) flavors</h3>
<p>The downside of passing serialization through multiple steps is that it is typically slower than
performing each step serially. Said simply, “cobs encoding while serializing” is often slower
than “serialize then cobs encode”, due to the ability to handle longer “runs” of data in each
stage. The downside is that if these stages can not be performed in-place on the buffer, you
will need additional buffers for each stage.</p>
<h3 id="examples"><a class="doc-anchor" href="#examples">§</a>Examples</h3><h4 id="using-a-single-flavor"><a class="doc-anchor" href="#using-a-single-flavor">§</a>Using a single flavor</h4>
<p>In the first example, we use the <code>Slice</code> flavor, to store the serialized output into a mutable <code>[u8]</code> slice.
No other modification is made to the serialization process.</p>

<div class="example-wrap"><pre class="rust rust-example-rendered"><code><span class="kw">use </span>postcard::{
    serialize_with_flavor,
    ser_flavors::Slice,
};

<span class="kw">let </span><span class="kw-2">mut </span>buf = [<span class="number">0u8</span>; <span class="number">32</span>];

<span class="kw">let </span>data: <span class="kw-2">&amp;</span>[u8] = <span class="kw-2">&amp;</span>[<span class="number">0x01</span>, <span class="number">0x00</span>, <span class="number">0x20</span>, <span class="number">0x30</span>];
<span class="kw">let </span>buffer = <span class="kw-2">&amp;mut </span>[<span class="number">0u8</span>; <span class="number">32</span>];
<span class="kw">let </span>res = serialize_with_flavor::&lt;[u8], Slice, <span class="kw-2">&amp;mut </span>[u8]&gt;(
    data,
    Slice::new(buffer)
).unwrap();

<span class="macro">assert_eq!</span>(res, <span class="kw-2">&amp;</span>[<span class="number">0x04</span>, <span class="number">0x01</span>, <span class="number">0x00</span>, <span class="number">0x20</span>, <span class="number">0x30</span>]);</code></pre></div>
<h4 id="using-combined-flavors"><a class="doc-anchor" href="#using-combined-flavors">§</a>Using combined flavors</h4>
<p>In the second example, we mix <code>Slice</code> with <code>Cobs</code>, to cobs encode the output while
the data is serialized. Notice how <code>Slice</code> (the storage flavor) is the innermost flavor used.</p>

<div class="example-wrap"><pre class="rust rust-example-rendered"><code><span class="kw">use </span>postcard::{
    serialize_with_flavor,
    ser_flavors::{Cobs, Slice},
};

<span class="kw">let </span><span class="kw-2">mut </span>buf = [<span class="number">0u8</span>; <span class="number">32</span>];

<span class="kw">let </span>data: <span class="kw-2">&amp;</span>[u8] = <span class="kw-2">&amp;</span>[<span class="number">0x01</span>, <span class="number">0x00</span>, <span class="number">0x20</span>, <span class="number">0x30</span>];
<span class="kw">let </span>buffer = <span class="kw-2">&amp;mut </span>[<span class="number">0u8</span>; <span class="number">32</span>];
<span class="kw">let </span>res = serialize_with_flavor::&lt;[u8], Cobs&lt;Slice&gt;, <span class="kw-2">&amp;mut </span>[u8]&gt;(
    data,
    Cobs::try_new(Slice::new(buffer)).unwrap(),
).unwrap();

<span class="macro">assert_eq!</span>(res, <span class="kw-2">&amp;</span>[<span class="number">0x03</span>, <span class="number">0x04</span>, <span class="number">0x01</span>, <span class="number">0x03</span>, <span class="number">0x20</span>, <span class="number">0x30</span>, <span class="number">0x00</span>]);</code></pre></div>
</div></details><h2 id="structs" class="section-header">Structs<a href="#structs" class="anchor">§</a></h2><ul class="item-table"><li><div class="item-name"><a class="struct" href="struct.Cobs.html" title="struct postcard::ser_flavors::Cobs">Cobs</a></div><div class="desc docblock-short">The <code>Cobs</code> flavor implements <a href="https://en.wikipedia.org/wiki/Consistent_Overhead_Byte_Stuffing">Consistent Overhead Byte Stuffing</a> on
the serialized data. The output of this flavor includes the termination/sentinel
byte of <code>0x00</code>.</div></li><li><div class="item-name"><a class="struct" href="struct.ExtendFlavor.html" title="struct postcard::ser_flavors::ExtendFlavor">Extend<wbr>Flavor</a></div><div class="desc docblock-short">Wrapper over a [<code>std::iter::Extend&lt;u8&gt;</code>] that implements the flavor trait</div></li><li><div class="item-name"><a class="struct" href="struct.HVec.html" title="struct postcard::ser_flavors::HVec">HVec</a></div><div class="desc docblock-short">The <code>HVec</code> flavor is a wrapper type around a <code>heapless::Vec</code>. This is a stack
allocated data structure, with a fixed maximum size and variable amount of contents.</div></li><li><div class="item-name"><a class="struct" href="struct.Size.html" title="struct postcard::ser_flavors::Size">Size</a></div><div class="desc docblock-short">The <code>Size</code> flavor is a measurement flavor, which accumulates the number of bytes needed to
serialize the data.</div></li><li><div class="item-name"><a class="struct" href="struct.Slice.html" title="struct postcard::ser_flavors::Slice">Slice</a></div><div class="desc docblock-short">The <code>Slice</code> flavor is a storage flavor, storing the serialized (or otherwise modified) bytes into a plain
<code>[u8]</code> slice. The <code>Slice</code> flavor resolves into a sub-slice of the original slice buffer.</div></li></ul><h2 id="traits" class="section-header">Traits<a href="#traits" class="anchor">§</a></h2><ul class="item-table"><li><div class="item-name"><a class="trait" href="trait.Flavor.html" title="trait postcard::ser_flavors::Flavor">Flavor</a></div><div class="desc docblock-short">The serialization Flavor trait</div></li></ul></section></div></main></body></html>