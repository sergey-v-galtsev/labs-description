<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="generator" content="rustdoc"><meta name="description" content="What `#[derive(Mul)]` generates"><title>Rem in derive_more - Rust</title><script>if(window.location.protocol!=="file:")document.head.insertAdjacentHTML("beforeend","SourceSerif4-Regular-46f98efaafac5295.ttf.woff2,FiraSans-Regular-018c141bf0843ffd.woff2,FiraSans-Medium-8f9a781e4970d388.woff2,SourceCodePro-Regular-562dcc5011b6de7d.ttf.woff2,SourceCodePro-Semibold-d899c5a5c4aeb14a.ttf.woff2".split(",").map(f=>`<link rel="preload" as="font" type="font/woff2" crossorigin href="../static.files/${f}">`).join(""))</script><link rel="stylesheet" href="../static.files/normalize-76eba96aa4d2e634.css"><link rel="stylesheet" href="../static.files/rustdoc-405f8b29f52305f8.css"><meta name="rustdoc-vars" data-root-path="../" data-static-root-path="../static.files/" data-current-crate="derive_more" data-themes="" data-resource-suffix="" data-rustdoc-version="1.83.0-nightly (26b5599e4 2024-09-06)" data-channel="nightly" data-search-js="search-a99f1315e7cc5121.js" data-settings-js="settings-7e3bb6c46e92e77c.js" ><script src="../static.files/storage-29b1d5a9048d38fe.js"></script><script defer src="sidebar-items.js"></script><script defer src="../static.files/main-14659ec02b58af51.js"></script><noscript><link rel="stylesheet" href="../static.files/noscript-40f72c9259523cb3.css"></noscript><link rel="alternate icon" type="image/png" href="../static.files/favicon-32x32-422f7d1d52889060.png"><link rel="icon" type="image/svg+xml" href="../static.files/favicon-2c020d218678b618.svg"></head><body class="rustdoc derive"><!--[if lte IE 11]><div class="warning">This old browser is unsupported and will most likely display funky things.</div><![endif]--><nav class="mobile-topbar"><button class="sidebar-menu-toggle" title="show sidebar"></button></nav><nav class="sidebar"><div class="sidebar-crate"><h2><a href="../derive_more/index.html">derive_<wbr>more</a><span class="version">1.0.0</span></h2></div><div class="sidebar-elems"><section id="rustdoc-toc"><h2 class="location"><a href="#">Rem</a></h2><h3><a href="#">Sections</a></h3><ul class="block top-toc"><li><a href="#what-derivemul-generates" title="What `#[derive(Mul)]` generates">What <code>#[derive(Mul)]</code> generates</a><ul><li><a href="#tuple-structs" title="Tuple structs">Tuple structs</a></li><li><a href="#regular-structs" title="Regular structs">Regular structs</a></li><li><a href="#enums" title="Enums">Enums</a></li></ul></li></ul></section><div id="rustdoc-modnav"><h2 class="in-crate"><a href="index.html">In crate derive_<wbr>more</a></h2></div></div></nav><div class="sidebar-resizer"></div><main><div class="width-limiter"><rustdoc-search></rustdoc-search><section id="main-content" class="content"><div class="main-heading"><h1>Derive Macro <a href="index.html">derive_more</a>::<wbr><a class="derive" href="#">Rem</a><button id="copy-path" title="Copy item path to clipboard">Copy item path</button></h1><span class="out-of-band"><a class="src" href="../src/derive_more_impl/lib.rs.html#103">source</a> · <button id="toggle-all-docs" title="collapse all docs">[<span>&#x2212;</span>]</button></span></div><pre class="rust item-decl"><code>#[derive(Rem)]
{
    <span class="comment">// Attributes available to this derive:</span>
    #[rem]
}
</code></pre><details class="toggle top-doc" open><summary class="hideme"><span>Expand description</span></summary><div class="docblock"><h2 id="what-derivemul-generates"><a class="doc-anchor" href="#what-derivemul-generates">§</a>What <code>#[derive(Mul)]</code> generates</h2>
<p>Deriving <code>Mul</code> is quite different from deriving <code>Add</code>. It is not used to
multiply two structs together. Instead it will normally multiply a struct, which
can have multiple fields, with a single primitive type (e.g. a <code>u64</code>). A new
struct is then created with all the fields from the previous struct multiplied
by this other value.</p>
<p>A simple way of explaining the reasoning behind this difference between <code>Add</code>
and <code>Mul</code> deriving, is looking at arithmetic on meters.
One meter can be added to one meter, to get two meters. Also, one meter times
two would be two meters, but one meter times one meter would be one square meter.
As this second case clearly requires more knowledge about the meaning of the
type in question deriving for this is not implemented.</p>
<p>NOTE: In case you don’t want this behaviour you can add <code>#[mul(forward)]</code> in
addition to <code>#[derive(Mul)]</code>. This will instead generate a <code>Mul</code> implementation
with the same semantics as <code>Add</code>.</p>
<h3 id="tuple-structs"><a class="doc-anchor" href="#tuple-structs">§</a>Tuple structs</h3>
<p>When deriving for a tuple struct with a single field (i.e. a newtype) like this:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered"><code><span class="attr">#[derive(Mul)]
</span><span class="kw">struct </span>MyInt(i32);</code></pre></div>
<p>Code like this will be generated:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered"><code><span class="kw">impl</span>&lt;__RhsT&gt; derive_more::Mul&lt;__RhsT&gt; <span class="kw">for </span>MyInt
    <span class="kw">where </span>i32: derive_more::Mul&lt;__RhsT, Output = i32&gt;
{
    <span class="kw">type </span>Output = MyInt;
    <span class="kw">fn </span>mul(<span class="self">self</span>, rhs: __RhsT) -&gt; MyInt {
        MyInt(<span class="self">self</span>.<span class="number">0</span>.mul(rhs))
    }
}</code></pre></div>
<p>The behaviour is slightly different for multiple fields, since the right hand
side of the multiplication now needs the <code>Copy</code> trait.
For instance when deriving for a tuple struct with two fields like this:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered"><code><span class="attr">#[derive(Mul)]
</span><span class="kw">struct </span>MyInts(i32, i32);</code></pre></div>
<p>Code like this will be generated:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered"><code><span class="kw">impl</span>&lt;__RhsT: Copy&gt; derive_more::Mul&lt;__RhsT&gt; <span class="kw">for </span>MyInts
    <span class="kw">where </span>i32: derive_more::Mul&lt;__RhsT, Output = i32&gt;
{
    <span class="kw">type </span>Output = MyInts;
    <span class="kw">fn </span>mul(<span class="self">self</span>, rhs: __RhsT) -&gt; MyInts {
        MyInts(<span class="self">self</span>.<span class="number">0</span>.mul(rhs), <span class="self">self</span>.<span class="number">1</span>.mul(rhs))
    }
}</code></pre></div>
<p>The behaviour is similar with more or less fields.</p>
<h3 id="regular-structs"><a class="doc-anchor" href="#regular-structs">§</a>Regular structs</h3>
<p>When deriving <code>Mul</code> for a regular struct with a single field like this:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered"><code><span class="attr">#[derive(Mul)]
</span><span class="kw">struct </span>Point1D {
    x: i32,
}</code></pre></div>
<p>Code like this will be generated:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered"><code><span class="kw">impl</span>&lt;__RhsT&gt; derive_more::Mul&lt;__RhsT&gt; <span class="kw">for </span>Point1D
    <span class="kw">where </span>i32: derive_more::Mul&lt;__RhsT, Output = i32&gt;
{
    <span class="kw">type </span>Output = Point1D;
    <span class="kw">fn </span>mul(<span class="self">self</span>, rhs: __RhsT) -&gt; Point1D {
        Point1D { x: <span class="self">self</span>.x.mul(rhs) }
    }
}</code></pre></div>
<p>The behaviour is again slightly different when deriving for a struct with multiple
fields, because it still needs the <code>Copy</code> as well.
For instance when deriving for a tuple struct with two fields like this:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered"><code><span class="attr">#[derive(Mul)]
</span><span class="kw">struct </span>Point2D {
    x: i32,
    y: i32,
}</code></pre></div>
<p>Code like this will be generated:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered"><code><span class="kw">impl</span>&lt;__RhsT: Copy&gt; derive_more::Mul&lt;__RhsT&gt; <span class="kw">for </span>Point2D
    <span class="kw">where </span>i32: derive_more::Mul&lt;__RhsT, Output = i32&gt;
{
    <span class="kw">type </span>Output = Point2D;
    <span class="kw">fn </span>mul(<span class="self">self</span>, rhs: __RhsT) -&gt; Point2D {
        Point2D {
            x: <span class="self">self</span>.x.mul(rhs),
            y: <span class="self">self</span>.y.mul(rhs),
        }
    }
}</code></pre></div>
<h3 id="enums"><a class="doc-anchor" href="#enums">§</a>Enums</h3>
<p>Deriving <code>Mul</code> for enums is not (yet) supported, except when you use
<code>#[mul(forward)]</code>.
Although it shouldn’t be impossible no effort has been put into this yet.</p>
</div></details></section></div></main></body></html>