<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="generator" content="rustdoc"><meta name="description" content="What `#[derive(Debug)]` generates"><title>Debug in derive_more::derive - Rust</title><script>if(window.location.protocol!=="file:")document.head.insertAdjacentHTML("beforeend","SourceSerif4-Regular-46f98efaafac5295.ttf.woff2,FiraSans-Regular-018c141bf0843ffd.woff2,FiraSans-Medium-8f9a781e4970d388.woff2,SourceCodePro-Regular-562dcc5011b6de7d.ttf.woff2,SourceCodePro-Semibold-d899c5a5c4aeb14a.ttf.woff2".split(",").map(f=>`<link rel="preload" as="font" type="font/woff2" crossorigin href="../../static.files/${f}">`).join(""))</script><link rel="stylesheet" href="../../static.files/normalize-76eba96aa4d2e634.css"><link rel="stylesheet" href="../../static.files/rustdoc-405f8b29f52305f8.css"><meta name="rustdoc-vars" data-root-path="../../" data-static-root-path="../../static.files/" data-current-crate="derive_more" data-themes="" data-resource-suffix="" data-rustdoc-version="1.83.0-nightly (26b5599e4 2024-09-06)" data-channel="nightly" data-search-js="search-a99f1315e7cc5121.js" data-settings-js="settings-7e3bb6c46e92e77c.js" ><script src="../../static.files/storage-29b1d5a9048d38fe.js"></script><script defer src="sidebar-items.js"></script><script defer src="../../static.files/main-14659ec02b58af51.js"></script><noscript><link rel="stylesheet" href="../../static.files/noscript-40f72c9259523cb3.css"></noscript><link rel="alternate icon" type="image/png" href="../../static.files/favicon-32x32-422f7d1d52889060.png"><link rel="icon" type="image/svg+xml" href="../../static.files/favicon-2c020d218678b618.svg"></head><body class="rustdoc derive"><!--[if lte IE 11]><div class="warning">This old browser is unsupported and will most likely display funky things.</div><![endif]--><nav class="mobile-topbar"><button class="sidebar-menu-toggle" title="show sidebar"></button></nav><nav class="sidebar"><div class="sidebar-crate"><h2><a href="../../derive_more/index.html">derive_<wbr>more</a><span class="version">1.0.0</span></h2></div><div class="sidebar-elems"><section id="rustdoc-toc"><h2 class="location"><a href="#">Debug</a></h2><h3><a href="#">Sections</a></h3><ul class="block top-toc"><li><a href="#what-derivedebug-generates" title="What `#[derive(Debug)]` generates">What <code>#[derive(Debug)]</code> generates</a><ul><li><a href="#the-format-of-the-format" title="The format of the format">The format of the format</a></li><li><a href="#example-usage" title="Example usage">Example usage</a></li></ul></li></ul></section><div id="rustdoc-modnav"><h2><a href="index.html">In derive_<wbr>more::<wbr>derive</a></h2></div></div></nav><div class="sidebar-resizer"></div><main><div class="width-limiter"><rustdoc-search></rustdoc-search><section id="main-content" class="content"><div class="main-heading"><h1>Derive Macro <a href="../index.html">derive_more</a>::<wbr><a href="index.html">derive</a>::<wbr><a class="derive" href="#">Debug</a><button id="copy-path" title="Copy item path to clipboard">Copy item path</button></h1><span class="out-of-band"><a class="src" href="../../src/derive_more_impl/lib.rs.html#103">source</a> · <button id="toggle-all-docs" title="collapse all docs">[<span>&#x2212;</span>]</button></span></div><pre class="rust item-decl"><code>#[derive(Debug)]
{
    <span class="comment">// Attributes available to this derive:</span>
    #[debug]
}
</code></pre><details class="toggle top-doc" open><summary class="hideme"><span>Expand description</span></summary><div class="docblock"><h2 id="what-derivedebug-generates"><a class="doc-anchor" href="#what-derivedebug-generates">§</a>What <code>#[derive(Debug)]</code> generates</h2>
<p>This derive macro is a clever superset of <code>Debug</code> from standard library. Additional features include:</p>
<ul>
<li>not imposing redundant trait bounds;</li>
<li><code>#[debug(skip)]</code> (or <code>#[debug(ignore)]</code>) attribute to skip formatting struct field or enum variant;</li>
<li><code>#[debug("...", args...)]</code> to specify custom formatting either for the whole struct or enum variant, or its particular field;</li>
<li><code>#[debug(bounds(...))]</code> to impose additional custom trait bounds.</li>
</ul>
<h3 id="the-format-of-the-format"><a class="doc-anchor" href="#the-format-of-the-format">§</a>The format of the format</h3>
<p>You supply a format by placing an attribute on a struct or enum variant, or its particular field:
<code>#[debug("...", args...)]</code>. The format is exactly like in <a href="https://doc.rust-lang.org/stable/std/macro.format.html"><code>format!()</code></a> or any other <a href="https://doc.rust-lang.org/stable/std/macro.format_args.html"><code>format_args!()</code></a>-based macros.</p>
<p>The variables available in the arguments is <code>self</code> and each member of the
struct or enum variant, with members of tuple structs being named with a
leading underscore and their index, i.e. <code>_0</code>, <code>_1</code>, <code>_2</code>, etc. Due to
ownership/lifetime limitations the member variables are all references to the
fields, except when used directly in the format string. For most purposes this
detail doesn’t matter, but it is quite important when using <code>Pointer</code>
formatting. If you don’t use the <code>{field:p}</code> syntax, you have to dereference
once to get the address of the field itself, instead of the address of the
reference to the field:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered"><code><span class="kw">use </span>derive_more::Debug;

<span class="attr">#[derive(Debug)]
#[debug(<span class="string">"{field:p} {:p}"</span>, <span class="kw-2">*</span>field)]
</span><span class="kw">struct </span>RefInt&lt;<span class="lifetime">'a</span>&gt; {
    field: <span class="kw-2">&amp;</span><span class="lifetime">'a </span>i32,
}

<span class="kw">let </span>a = <span class="kw-2">&amp;</span><span class="number">123</span>;
<span class="macro">assert_eq!</span>(<span class="macro">format!</span>(<span class="string">"{:?}"</span>, RefInt{field: <span class="kw-2">&amp;</span>a}), <span class="macro">format!</span>(<span class="string">"{a:p} {:p}"</span>, a));</code></pre></div>
<h4 id="generic-data-types"><a class="doc-anchor" href="#generic-data-types">§</a>Generic data types</h4>
<p>When deriving <code>Debug</code> for a generic struct/enum, all generic type arguments <em>used</em> during formatting
are bound by respective formatting trait.</p>
<p>E.g., for a structure <code>Foo</code> defined like this:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered"><code><span class="kw">use </span>derive_more::Debug;

<span class="attr">#[derive(Debug)]
</span><span class="kw">struct </span>Foo&lt;<span class="lifetime">'a</span>, T1, T2: Trait, T3, T4&gt; {
    <span class="attr">#[debug(<span class="string">"{a}"</span>)]
    </span>a: T1,
    <span class="attr">#[debug(<span class="string">"{b}"</span>)]
    </span>b: &lt;T2 <span class="kw">as </span>Trait&gt;::Type,
    <span class="attr">#[debug(<span class="string">"{c:?}"</span>)]
    </span>c: Vec&lt;T3&gt;,
    <span class="attr">#[debug(<span class="string">"{d:p}"</span>)]
    </span>d: <span class="kw-2">&amp;</span><span class="lifetime">'a </span>T1,
    <span class="attr">#[debug(skip)] </span><span class="comment">// or #[debug(ignore)]
    </span>e: T4,
}

<span class="kw">trait </span>Trait { <span class="kw">type </span>Type; }</code></pre></div>
<p>The following where clauses would be generated:</p>
<ul>
<li><code>T1: Display</code></li>
<li><code>&lt;T2 as Trait&gt;::Type: Display</code></li>
<li><code>Vec&lt;T3&gt;: Debug</code></li>
<li><code>&amp;'a T1: Pointer</code></li>
</ul>
<h4 id="custom-trait-bounds"><a class="doc-anchor" href="#custom-trait-bounds">§</a>Custom trait bounds</h4>
<p>Sometimes you may want to specify additional trait bounds on your generic type parameters, so that they could be used
during formatting. This can be done with a <code>#[debug(bound(...))]</code> attribute.</p>
<p><code>#[debug(bound(...))]</code> accepts code tokens in a format similar to the format used in angle bracket list (or <code>where</code>
clause predicates): <code>T: MyTrait, U: Trait1 + Trait2</code>.</p>
<p>Using <code>#[debug("...", ...)]</code> formatting we’ll try our best to infer trait bounds, but in more advanced cases this isn’t
possible. Our aim is to avoid imposing additional bounds, as they can be added with <code>#[debug(bound(...))]</code>.
In the example below, we can infer only that <code>V: Display</code>, other bounds have to be supplied by the user:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered"><code><span class="kw">use </span>std::fmt::Display;
<span class="kw">use </span>derive_more::Debug;

<span class="attr">#[derive(Debug)]
#[debug(bound(T: MyTrait, U: Display))]
</span><span class="kw">struct </span>MyStruct&lt;T, U, V, F&gt; {
    <span class="attr">#[debug(<span class="string">"{}"</span>, a.my_function())]
    </span>a: T,
    <span class="attr">#[debug(<span class="string">"{}"</span>, b.to_string().len())]
    </span>b: U,
    <span class="attr">#[debug(<span class="string">"{c}"</span>)]
    </span>c: V,
    <span class="attr">#[debug(skip)] </span><span class="comment">// or #[debug(ignore)]
    </span>d: F,
}

<span class="kw">trait </span>MyTrait { <span class="kw">fn </span>my_function(<span class="kw-2">&amp;</span><span class="self">self</span>) -&gt; i32; }</code></pre></div>
<h4 id="transparency"><a class="doc-anchor" href="#transparency">§</a>Transparency</h4>
<p>If the top-level <code>#[debug("...", args...)]</code> attribute (the one for a whole struct or variant) is specified
and can be trivially substituted with a transparent delegation call to the inner type, then all the additional
<a href="https://doc.rust-lang.org/stable/std/fmt/index.html#formatting-parameters">formatting parameters</a> do work as expected:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered"><code><span class="kw">use </span>derive_more::Debug;

<span class="attr">#[derive(Debug)]
#[debug(<span class="string">"{_0:o}"</span>)] </span><span class="comment">// the same as calling `Octal::fmt()`
</span><span class="kw">struct </span>MyOctalInt(i32);

<span class="comment">// so, additional formatting parameters do work transparently
</span><span class="macro">assert_eq!</span>(<span class="macro">format!</span>(<span class="string">"{:03?}"</span>, MyOctalInt(<span class="number">9</span>)), <span class="string">"011"</span>);

<span class="attr">#[derive(Debug)]
#[debug(<span class="string">"{_0:02b}"</span>)]     </span><span class="comment">// cannot be trivially substituted with `Binary::fmt()`,
</span><span class="kw">struct </span>MyBinaryInt(i32); <span class="comment">// because of specified formatting parameters

// so, additional formatting parameters have no effect
</span><span class="macro">assert_eq!</span>(<span class="macro">format!</span>(<span class="string">"{:07?}"</span>, MyBinaryInt(<span class="number">2</span>)), <span class="string">"10"</span>);</code></pre></div>
<p>If, for some reason, transparency in trivial cases is not desired, it may be suppressed explicitly
either with the <a href="https://doc.rust-lang.org/stable/std/macro.format_args.html"><code>format_args!()</code></a> macro usage:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered"><code><span class="kw">use </span>derive_more::Debug;

<span class="attr">#[derive(Debug)]
#[debug(<span class="string">"{}"</span>, <span class="macro">format_args!</span>(<span class="string">"{_0:o}"</span>))] </span><span class="comment">// `format_args!()` obscures the inner type
</span><span class="kw">struct </span>MyOctalInt(i32);

<span class="comment">// so, additional formatting parameters have no effect
</span><span class="macro">assert_eq!</span>(<span class="macro">format!</span>(<span class="string">"{:07?}"</span>, MyOctalInt(<span class="number">9</span>)), <span class="string">"11"</span>);</code></pre></div>
<p>Or by adding <a href="https://doc.rust-lang.org/stable/std/fmt/index.html#formatting-parameters">formatting parameters</a> which cause no visual effects:</p>

<div class="example-wrap"><pre class="rust rust-example-rendered"><code><span class="kw">use </span>derive_more::Debug;

<span class="attr">#[derive(Debug)]
#[debug(<span class="string">"{_0:^o}"</span>)] </span><span class="comment">// `^` is centering, but in absence of additional width has no effect
</span><span class="kw">struct </span>MyOctalInt(i32);

<span class="comment">// and so, additional formatting parameters have no effect
</span><span class="macro">assert_eq!</span>(<span class="macro">format!</span>(<span class="string">"{:07?}"</span>, MyOctalInt(<span class="number">9</span>)), <span class="string">"11"</span>);</code></pre></div>
<h3 id="example-usage"><a class="doc-anchor" href="#example-usage">§</a>Example usage</h3>
<div class="example-wrap"><pre class="rust rust-example-rendered"><code><span class="kw">use </span>std::path::PathBuf;
<span class="kw">use </span>derive_more::Debug;

<span class="attr">#[derive(Debug)]
</span><span class="kw">struct </span>MyInt(i32);

<span class="attr">#[derive(Debug)]
</span><span class="kw">struct </span>MyIntHex(<span class="attr">#[debug(<span class="string">"{_0:x}"</span>)] </span>i32);

<span class="attr">#[derive(Debug)]
#[debug(<span class="string">"{_0} = {_1}"</span>)]
</span><span class="kw">struct </span>StructFormat(<span class="kw-2">&amp;</span><span class="lifetime">'static </span>str, u8);

<span class="attr">#[derive(Debug)]
</span><span class="kw">enum </span>E {
    Skipped {
        x: u32,
        <span class="attr">#[debug(skip)] </span><span class="comment">// or #[debug(ignore)]
        </span>y: u32,
    },
    Binary {
        <span class="attr">#[debug(<span class="string">"{i:b}"</span>)]
        </span>i: i8,
    },
    Path(<span class="attr">#[debug(<span class="string">"{}"</span>, _0.display())] </span>PathBuf),
    <span class="attr">#[debug(<span class="string">"{_0}"</span>)]
    </span>EnumFormat(bool)
}

<span class="macro">assert_eq!</span>(<span class="macro">format!</span>(<span class="string">"{:?}"</span>, MyInt(-<span class="number">2</span>)), <span class="string">"MyInt(-2)"</span>);
<span class="macro">assert_eq!</span>(<span class="macro">format!</span>(<span class="string">"{:?}"</span>, MyIntHex(-<span class="number">255</span>)), <span class="string">"MyIntHex(ffffff01)"</span>);
<span class="macro">assert_eq!</span>(<span class="macro">format!</span>(<span class="string">"{:?}"</span>, StructFormat(<span class="string">"answer"</span>, <span class="number">42</span>)), <span class="string">"answer = 42"</span>);
<span class="macro">assert_eq!</span>(<span class="macro">format!</span>(<span class="string">"{:?}"</span>, E::Skipped { x: <span class="number">10</span>, y: <span class="number">20 </span>}), <span class="string">"Skipped { x: 10, .. }"</span>);
<span class="macro">assert_eq!</span>(<span class="macro">format!</span>(<span class="string">"{:?}"</span>, E::Binary { i: -<span class="number">2 </span>}), <span class="string">"Binary { i: 11111110 }"</span>);
<span class="macro">assert_eq!</span>(<span class="macro">format!</span>(<span class="string">"{:?}"</span>, E::Path(<span class="string">"abc"</span>.into())), <span class="string">"Path(abc)"</span>);
<span class="macro">assert_eq!</span>(<span class="macro">format!</span>(<span class="string">"{:?}"</span>, E::EnumFormat(<span class="bool-val">true</span>)), <span class="string">"true"</span>);</code></pre></div>
</div></details></section></div></main></body></html>