(function() {
    var implementors = Object.fromEntries([["bga",[["impl <a class=\"trait\" href=\"embedded_graphics_core/pixelcolor/trait.PixelColor.html\" title=\"trait embedded_graphics_core::pixelcolor::PixelColor\">PixelColor</a> for <a class=\"struct\" href=\"bga/color/struct.Rgb565.html\" title=\"struct bga::color::Rgb565\">Rgb565</a>"]]],["embedded_graphics",[]],["embedded_graphics_core",[]]]);
    if (window.register_implementors) {
        window.register_implementors(implementors);
    } else {
        window.pending_implementors = implementors;
    }
})()
//{"start":57,"fragment_lengths":[289,25,30]}