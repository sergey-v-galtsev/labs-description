(function() {
    var implementors = Object.fromEntries([["kernel",[["impl <a class=\"trait\" href=\"ku/allocator/big/trait.BigAllocator.html\" title=\"trait ku::allocator::big::BigAllocator\">BigAllocator</a> for <a class=\"struct\" href=\"kernel/allocator/big/struct.Big.html\" title=\"struct kernel::allocator::big::Big\">Big</a>&lt;'_&gt;"]]],["lib",[["impl <a class=\"trait\" href=\"ku/allocator/big/trait.BigAllocator.html\" title=\"trait ku::allocator::big::BigAllocator\">BigAllocator</a> for <a class=\"struct\" href=\"lib/allocator/map/struct.MapAllocator.html\" title=\"struct lib::allocator::map::MapAllocator\">MapAllocator</a>"]]]]);
    if (window.register_implementors) {
        window.register_implementors(implementors);
    } else {
        window.pending_implementors = implementors;
    }
})()
//{"start":57,"fragment_lengths":[289,298]}