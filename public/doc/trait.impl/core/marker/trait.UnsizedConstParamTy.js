(function() {
    var implementors = Object.fromEntries([["ku",[["impl <a class=\"trait\" href=\"https://doc.rust-lang.org/nightly/core/marker/trait.UnsizedConstParamTy.html\" title=\"trait core::marker::UnsizedConstParamTy\">UnsizedConstParamTy</a> for <a class=\"enum\" href=\"ku/sync/panic/enum.PanicStrategy.html\" title=\"enum ku::sync::panic::PanicStrategy\">PanicStrategy</a>"]]]]);
    if (window.register_implementors) {
        window.register_implementors(implementors);
    } else {
        window.pending_implementors = implementors;
    }
})()
//{"start":57,"fragment_lengths":[329]}