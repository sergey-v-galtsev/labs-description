<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="generator" content="rustdoc"><meta name="description" content="Embedded-friendly (i.e. `#![no_std]`) math library featuring fast, safe floating point approximations for common arithmetic operations, as well as 2D and 3D vector types, statistical analysis functions, and quaternions."><title>micromath - Rust</title><script>if(window.location.protocol!=="file:")document.head.insertAdjacentHTML("beforeend","SourceSerif4-Regular-46f98efaafac5295.ttf.woff2,FiraSans-Regular-018c141bf0843ffd.woff2,FiraSans-Medium-8f9a781e4970d388.woff2,SourceCodePro-Regular-562dcc5011b6de7d.ttf.woff2,SourceCodePro-Semibold-d899c5a5c4aeb14a.ttf.woff2".split(",").map(f=>`<link rel="preload" as="font" type="font/woff2" crossorigin href="../static.files/${f}">`).join(""))</script><link rel="stylesheet" href="../static.files/normalize-76eba96aa4d2e634.css"><link rel="stylesheet" href="../static.files/rustdoc-405f8b29f52305f8.css"><meta name="rustdoc-vars" data-root-path="../" data-static-root-path="../static.files/" data-current-crate="micromath" data-themes="" data-resource-suffix="" data-rustdoc-version="1.83.0-nightly (26b5599e4 2024-09-06)" data-channel="nightly" data-search-js="search-a99f1315e7cc5121.js" data-settings-js="settings-7e3bb6c46e92e77c.js" ><script src="../static.files/storage-29b1d5a9048d38fe.js"></script><script defer src="../crates.js"></script><script defer src="../static.files/main-14659ec02b58af51.js"></script><noscript><link rel="stylesheet" href="../static.files/noscript-40f72c9259523cb3.css"></noscript><link rel="alternate icon" type="image/png" href="../static.files/favicon-32x32-422f7d1d52889060.png"><link rel="icon" type="image/svg+xml" href="../static.files/favicon-2c020d218678b618.svg"></head><body class="rustdoc mod crate"><!--[if lte IE 11]><div class="warning">This old browser is unsupported and will most likely display funky things.</div><![endif]--><nav class="mobile-topbar"><button class="sidebar-menu-toggle" title="show sidebar"></button><a class="logo-container" href="../micromath/index.html"><img src="https://raw.githubusercontent.com/tarcieri/micromath/main/img/micromath-sq.png" alt=""></a></nav><nav class="sidebar"><div class="sidebar-crate"><a class="logo-container" href="../micromath/index.html"><img src="https://raw.githubusercontent.com/tarcieri/micromath/main/img/micromath-sq.png" alt="logo"></a><h2><a href="../micromath/index.html">micromath</a><span class="version">1.1.1</span></h2></div><div class="sidebar-elems"><ul class="block"><li><a id="all-types" href="all.html">All Items</a></li></ul><section id="rustdoc-toc"><h3><a href="#">Sections</a></h3><ul class="block top-toc"><li><a href="#floating-point-approximations" title="Floating point approximations">Floating point approximations</a><ul><li><a href="#unused-import-warnings-when-linking-std" title="Unused import warnings when linking `std`">Unused import warnings when linking <code>std</code></a></li></ul></li><li><a href="#vector-types" title="Vector types">Vector types</a></li><li><a href="#statistical-analysis" title="Statistical analysis">Statistical analysis</a></li><li><a href="#quaternions" title="Quaternions">Quaternions</a></li></ul><h3><a href="#traits">Crate Items</a></h3><ul class="block"><li><a href="#traits" title="Traits">Traits</a></li></ul></section><div id="rustdoc-modnav"></div></div></nav><div class="sidebar-resizer"></div><main><div class="width-limiter"><rustdoc-search></rustdoc-search><section id="main-content" class="content"><div class="main-heading"><h1>Crate <a class="mod" href="#">micromath</a><button id="copy-path" title="Copy item path to clipboard">Copy item path</button></h1><span class="out-of-band"><a class="src" href="../src/micromath/lib.rs.html#1-118">source</a> · <button id="toggle-all-docs" title="collapse all docs">[<span>&#x2212;</span>]</button></span></div><details class="toggle top-doc" open><summary class="hideme"><span>Expand description</span></summary><div class="docblock"><p>Embedded-friendly (i.e. <code>#![no_std]</code>) math library featuring fast, safe
floating point approximations for common arithmetic operations, as well as
2D and 3D vector types, statistical analysis functions, and quaternions.</p>
<h3 id="floating-point-approximations"><a class="doc-anchor" href="#floating-point-approximations">§</a>Floating point approximations</h3>
<p><code>micromath</code> supports approximating many arithmetic operations on <code>f32</code>
using bitwise operations, providing great performance and small code size
at the cost of precision. For use cases like graphics and signal
processing, these approximations are often sufficient and the performance
gains worth the lost precision.</p>
<p>These approximations are provided by the <a href="https://docs.rs/micromath/latest/micromath/trait.F32Ext.html">micromath::F32Ext</a> trait which is
impl’d for <code>f32</code>, providing a drop-in <code>std</code>-compatible (sans lost precision) API.</p>

<div class="example-wrap"><pre class="rust rust-example-rendered"><code><span class="kw">use </span>micromath::F32Ext;

<span class="kw">let </span>n = <span class="number">2.0</span>.sqrt();
<span class="macro">assert_eq!</span>(n, <span class="number">1.5</span>); <span class="comment">// close enough</span></code></pre></div>
<h4 id="unused-import-warnings-when-linking-std"><a class="doc-anchor" href="#unused-import-warnings-when-linking-std">§</a>Unused import warnings when linking <code>std</code></h4>
<p>Since the <code>F32Ext</code> trait provides methods which are already defined in
<code>std</code>, in cases where your crate links <code>std</code> the <code>F32Ext</code> versions of
the same methods will not be used, in which case you will get an unused
import warning for <code>F32Ext</code>.</p>
<p>If you encounter this, add an <code>#[allow(unused_imports)]</code> above the import.</p>

<div class="example-wrap"><pre class="rust rust-example-rendered"><code><span class="attr">#[allow(unused_imports)]
</span><span class="kw">use </span>micromath::F32Ext;</code></pre></div>
<h3 id="vector-types"><a class="doc-anchor" href="#vector-types">§</a>Vector types</h3>
<p>See the <a href="https://docs.rs/micromath/latest/micromath/vector/index.html"><code>vector</code> module</a> for more information on vector types.</p>
<p>The following vector types are available, all of which have <code>pub x</code> and
<code>pub y</code> (and on 3D vectors, <code>pub z</code>) members:</p>
<div><table><thead><tr><th>Rust</th><th>2D</th><th>3D</th></tr></thead><tbody>
<tr><td><code>i8</code></td><td><code>I8x2</code></td><td><code>I8x3</code></td></tr>
<tr><td><code>i16</code></td><td><code>I16x2</code></td><td><code>I16x3</code></td></tr>
<tr><td><code>i32</code></td><td><code>I32x2</code></td><td><code>I32x3</code></td></tr>
<tr><td><code>u8</code></td><td><code>U8x2</code></td><td><code>U8x3</code></td></tr>
<tr><td><code>u16</code></td><td><code>U16x2</code></td><td><code>U16x3</code></td></tr>
<tr><td><code>u32</code></td><td><code>U32x2</code></td><td><code>U32x3</code></td></tr>
<tr><td><code>f32</code></td><td><code>F32x2</code></td><td><code>F32x3</code></td></tr>
</tbody></table>
</div><h3 id="statistical-analysis"><a class="doc-anchor" href="#statistical-analysis">§</a>Statistical analysis</h3>
<p>See the <a href="https://docs.rs/micromath/latest/micromath/statistics/index.html"><code>statistics</code> module</a> for more information on statistical analysis
traits and functionality.</p>
<p>The following traits are available and impl’d for slices and iterators of
<code>f32</code> (and can be impl’d for other types):</p>
<ul>
<li><a href="https://docs.rs/micromath/latest/micromath/statistics/trait.Mean.html">Mean</a> - compute arithmetic mean with the <code>mean()</code> method</li>
<li><a href="https://docs.rs/micromath/latest/micromath/statistics/trait.StdDev.html">StdDev</a> - compute standard deviation with the <code>stddev()</code> method</li>
<li><a href="https://docs.rs/micromath/latest/micromath/statistics/trim/trait.Trim.html">Trim</a> - cull outliers from a sample slice with the <code>trim()</code> method.</li>
<li><a href="https://docs.rs/micromath/latest/micromath/statistics/trait.Variance.html">Variance</a> - compute variance with the `variance() method</li>
</ul>
<h3 id="quaternions"><a class="doc-anchor" href="#quaternions">§</a>Quaternions</h3>
<p>See the <a href="https://docs.rs/micromath/latest/micromath/quaternion/index.html"><code>quaternion</code> module</a> for more information.</p>
</div></details><h2 id="traits" class="section-header">Traits<a href="#traits" class="anchor">§</a></h2><ul class="item-table"><li><div class="item-name"><a class="trait" href="trait.F32Ext.html" title="trait micromath::F32Ext">F32Ext</a></div><div class="desc docblock-short"><code>f32</code> extension providing various arithmetic approximations and polyfills
for <code>std</code> functionality.</div></li></ul></section></div></main></body></html>